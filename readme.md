#### FFmpeg的命令 

合并ts文件 首先需要一个filelist，里面的内容形如
```
file '1588589998237-1588590003862.ts'
file '1588590003862-1588590009487.ts'
file '1588590009490-1588590015115.ts'
file '1588590015199-1588590020824.ts'
file '1588590020763-1588590026386.ts'
file '1588590026395-1588590032023.ts'
file '1588590032041-1588590037666.ts'
```

里面必须用`'`单引号，并且最好是这样的相对路径，windows的绝对路径他好像不能使用

命令"=
```
ffmpeg.exe -f concat -i filelist.txt -c copy 0505-行测-数量关系.TS
```

参考来源：
https://superuser.com/questions/692990/use-ffmpeg-copy-codec-to-combine-ts-files-into-a-single-mp4

https://stackoverflow.com/questions/7333232/how-to-concatenate-two-mp4-files-using-ffmpeg

